import {useState, useEffect} from 'react'
import {Container, Form, Button} from 'react-bootstrap'

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect( () => {

		if(email !== "" && password !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [email, password])

	function Login(e){
		e.preventDefault()
		localStorage.setItem(`email`,email)
		setEmail("")
		setPassword("")

		alert("Login Successfully!")
	}

	return(

		<Container>
			<Form 
				className="border p-3 mb-3"
				onSubmit={ (e) => Login(e) }
				>
			{/*email*/}
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email} 
			    	onChange={ (e) => setEmail(e.target.value) }
			    	/>
			  </Form.Group>
			{/*password*/}
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password} 
			    	onChange={ (e) => setPassword(e.target.value) }
			    	/>
			  </Form.Group>
			  
			  <Button 
			  		variant="primary" 
			  		type="submit"
			  		disabled={isDisabled}
			  		>
			    Submit
			  </Button>
			</Form>
		</Container>

	)
}
