/*import {Fragment} from 'react'*/
import {Container} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'; 
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

export default function App() {

  return(

      <BrowserRouter>
        <AppNavbar/>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/Courses" component={Courses}/>
            <Route exact path="/Register" component={Register}/>
            <Route exact path="/Logout" component={Logout}/>
            <Route exact path="/Login" component={Login}/>
            <Route component={Error} />
        </Switch>
      </BrowserRouter>
    )
}

