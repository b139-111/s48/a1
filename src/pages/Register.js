import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';

export default function Register(){

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cpw, setCpw] = useState('');
  const [isDisabled, setIsDisabled] = useState(false);

useEffect( () => {
  if(email !== "" && password !== "" && cpw !== "" && (password === cpw)){
    setIsDisabled(false)
  } else {
    setIsDisabled(true)
  }

}, [email, password, cpw])

function  Register(e){
  e.preventDefault()
  alert("Registered Successfully") 
}
return(

  <Container>
    <Form 
      className="border p-3 mb-3"
      onSubmit={ (e) => Register(e)}
      >
      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter Email" 
          value={email} 
          onChange={ (e) => setEmail(e.target.value) }
          />
      </Form.Group>
      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Enter Password" 
          value={password}
          onChange={ (e) => setPassword(e.target.value) }
          />
      </Form.Group>
      <Form.Group className="mb-3" controlId="cpw">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Verify Password" 
          value={cpw}
          onChange={ (e) => setCpw(e.target.value) }
          />
      </Form.Group>
      <Button 
      variant="primary" 
      type="submit"
      disabled={isDisabled}
      >
        Submit
      </Button>
    </Form>
  </Container>
)}