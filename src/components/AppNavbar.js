
import {useState, Fragment} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'


export default function AppNavbar() {

  const [user, setUser] = useState(localStorage.getItem("email"))



  let leftNav = (user != null) ? 
    <Nav.Link as={NavLink} to="/Logout">Logout</Nav.Link>
  : 
    <Fragment>
      <Nav.Link as={NavLink} to="/Register">Register</Nav.Link>
      <Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
    </Fragment>


  return(

      <Navbar bg="primary" expand="lg">
        <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/Courses">Courses</Nav.Link>
          </Nav>
          <Nav>
            {leftNav}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
}

